#
# UEA Quantum Fluids group
#
# Spectral integration of the two-dimensional Gross-Pitaevskii (GP) equation,
# i\frac{\partial\psi}{\partial t} + \beta\nabla\psi + \alpha|\psi|^2\psi = 0,
# with no external potential and periodic boundary conditions.
#
# Time integrator: first order split-step method.
# Boundary conditions: periodic
#


# Load python modules
import numpy as np
import os
import shutil
import matplotlib
matplotlib.use('Agg')


# Load other specific modules
import initialConditions as ic
import plotFields as pf


# Set simulation parameters
alpha=-1./2. 						# nonlinear coefficient (set as this to use Pade' coefficients from Berloff)
beta=1./2. 							# linear coefficient (set as this to use Pade' coefficients from Berloff)

nx=256                              # number of uniform points along the x-axis
ny=256                              # number of uniform points along the y-axis
lx=100.                             # spatial domain length in the x-direction
ly=100.                             # spatial domain length in the y-direction

dt=2.e-02                           # integration time-step
initialStep=0                       # initial step
finalStep=20000                     # final step

conSave=1000                          # steps between successive save of conserved quantities
psiSave=1000                        # steps between successive save of field psi

dirOutputs='./outputs'                  # directory where to store the outputs
dirFields='%s/fields/' % (dirOutputs)    # directory where to store the binary fields
dirPlots='%s/plots/' % ( dirOutputs)    # directory where to store the plots

print('parameters set')


# Set other constants
dx=lx/nx                            # uniform grid step along the x-axis
dy=ly/ny                            # uniform grid step along the y-axis
dV=dx*dy

x=np.linspace(0., lx, num=nx, endpoint=False)
y=np.linspace(0., ly, num=ny, endpoint=False)

dkx=2.*np.pi/lx
dky=2.*np.pi/ly

kx=np.fft.ifftshift(dkx*np.arange(-nx/2, nx/2))
ky=np.fft.ifftshift(dky*np.arange(-ny/2, ny/2))

print('other constants set')


# Define directory where to store outputs and plots
if not os.path.exists(dirOutputs):
    os.makedirs(dirOutputs)
if not os.path.exists(dirFields):
    os.makedirs(dirFields)
if not os.path.exists(dirPlots):
    os.makedirs(dirPlots)

print('auxiliary directories created')


# Create |k|^2 two-dimensioanal array
kSquare=np.square(np.kron(kx, np.ones(ny)).reshape(nx, ny))
kSquare=kSquare+np.square(np.kron(np.ones(nx), ky).reshape(nx, ny))

print('kSquare array created')


# Define exponent term for linear evolution
lin_exp=np.exp(-1j*beta*kSquare*dt)

print('lin_exp array defined')


# Compare fastest linear period and time-step
print('fastest linear period=', 2.*np.pi/(beta*np.max(kSquare)))
print('time-step dt=', dt)


# Set initial condition

# create a uniform condensate where the density is equal to 1
psi=np.ones((nx, ny), dtype=np.complex)

# add random noise
# psi=psi*np.random.rand(nx ,ny)+0.01*1j**np.random.rand(nx ,ny)     # random initial condition

# add 4 vortex points
vortexPositions=[]
vortexPositions.append(np.array([3*lx/10., ly/2.-5.]))
vortexPositions.append(np.array([3*lx/10., ly/2.+5.]))
vortexPositions.append(np.array([7*lx/10., ly/2.-5.]))
vortexPositions.append(np.array([7*lx/10., ly/2.+5.]))

vortexWindingNumbers=[]
vortexWindingNumbers.append(1)
vortexWindingNumbers.append(-1)
vortexWindingNumbers.append(-1)
vortexWindingNumbers.append(+1)

#psi=ic.vortexPoints2D(lx, ly, nx, ny, x, y, psi=psi)
psi=ic.vortexPoints2D(lx, ly, nx, ny, x, y, vortexPositions=vortexPositions, vortexWindingNumbers=vortexWindingNumbers, psi=psi)

print('initial condition for psi set')


# Erase previous simulation conserved quantities
filename='%s/Mass.txt' % (dirOutputs)
if os.path.isfile(filename):
    os.remove(filename)

filename='%s/Momentum.txt' % (dirOutputs)
if os.path.isfile(filename):
    os.remove(filename)

filename='%s/Energy.txt' % (dirOutputs)
if os.path.isfile(filename):
    os.remove(filename)


# Save conserved quantities
step=initialStep
if np.mod(step, conSave)==0:
    
    # Mass
    mass=np.sum(np.abs(psi)**2)*dV
    
    # Nonlinear energy
    nl_energy=-(alpha/2.)*np.sum(np.abs(psi)**4)*dV
        
    # Transform forward into Fourier space (in-place transform)
    psi=np.fft.fft2(psi)
        
    # Linear energy
    lin_energy=beta*np.sum(kSquare*np.abs(psi)**2)*dV
        
    # Linear momentum components
    px=beta*np.sum(np.kron(kx, np.ones(ny)).reshape(nx, ny)*np.abs(psi)**2)*dV
    py=beta*np.sum(np.kron(np.ones(nx), ky).reshape(nx, ny)*np.abs(psi)**2)*dV
        
    # Transform backward into physical space (in-place transform)
    psi=np.fft.ifft2(psi)
        
    # Save conserved quantities
    fname='%s/Mass.txt' % (dirOutputs)
    with open(fname, 'ab') as opat:
        np.savetxt(opat, [[step*dt, mass]])
        
    fname='%s/Momentum.txt' % (dirOutputs)
    with open(fname, 'ab') as opat:
        np.savetxt(opat, [[step*dt, px, py]])
        
    fname='%s/Energy.txt' % (dirOutputs)
    with open(fname, 'ab') as opat:
        np.savetxt(opat, [[step*dt, lin_energy+nl_energy, lin_energy, nl_energy]])


# Save field psi
if np.mod(step, psiSave)==0:
    fname='%s/psi%05d.npy' % (dirFields, step/psiSave)
    with open(fname, 'ab') as opab:
        np.save(opab, psi)
    print(fname)
        
    filename='%s/density%05d.png' % (dirPlots, step/psiSave)
    pf.density_imshow2D(psi, filename=filename)
        
    filename='%s/phase%05d.png' % (dirPlots, step/psiSave)
    pf.phase_imshow2D(psi, filename=filename)


# Integration loop
print('integration loop begins')
for step in range(initialStep+1, finalStep+1):


    # Evolve nonlinear part in physical space
    psi=psi*np.exp(1j*alpha*dt*np.abs(psi)**2)
 
 
    # Transform forward into Fourier space (in-place transform)
    psi=np.fft.fft2(psi)
  
  
    # Evolve linear part in Fourier space
    psi=psi*lin_exp


    # Transform backward into physical space (in-place transform)
    psi=np.fft.ifft2(psi)
    
    
    # Save conserved quantities
    if np.mod(step, conSave)==0:
        
        # Mass
        mass=np.sum(np.abs(psi)**2)*dV
        
        # Nonlinear energy
        nl_energy=-(alpha/2.)*np.sum(np.abs(psi)**4)*dV
        
        # Transform forward into Fourier space (in-place transform)
        psi=np.fft.fft2(psi)

        # Linear energy
        lin_energy=beta*np.sum(kSquare*np.abs(psi)**2)*dV
    
        # Linear momentum components
        px=beta*np.sum(np.kron(kx, np.ones(ny)).reshape(nx, ny)*np.abs(psi)**2)*dV
        py=beta*np.sum(np.kron(np.ones(nx), ky).reshape(nx, ny)*np.abs(psi)**2)*dV
    
        # Transform backward into physical space (in-place transform)
        psi=np.fft.ifft2(psi)
        
        # Save conserved quantities
        fname='%s/Mass.txt' % (dirOutputs)
        with open(fname, 'ab') as opat:
            np.savetxt(opat, [[step*dt, mass]])
        
        fname='%s/Momentum.txt' % (dirOutputs)
        with open(fname, 'ab') as opat:
            np.savetxt(opat, [[step*dt, px, py]])
        
        fname='%s/Energy.txt' % (dirOutputs)
        with open(fname, 'ab') as opat:
            np.savetxt(opat, [[step*dt, lin_energy+nl_energy, lin_energy, nl_energy]])


    # Save field psi
    if np.mod(step, psiSave)==0:
        fname='%s/psi%05d.npy' % (dirFields, step/psiSave)
        with open(fname, 'ab') as opab:
            np.save(opab, psi)
        print(fname)

        filename='%s/density%05d.png' % (dirPlots, step/psiSave)
        pf.density_imshow2D(psi, filename=filename)

        filename='%s/phase%05d.png' % (dirPlots, step/psiSave)
        pf.phase_imshow2D(psi, filename=filename)


# End of integration loop
print('integration loop ends')








