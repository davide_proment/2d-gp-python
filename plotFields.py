#
# UEA Quantum Fluids group
#
# Routines to make various plots of the fields
#
# Boundary conditions: periodic
#


# Load python modules
import matplotlib.pyplot as plt
import numpy as np 


# Routines

def density_imshow2D(psi, filename=None, figsize=(8, 8), dpi=200, axes_labels=['nx', 'ny']):
    """
        Plot the density of psi and either save image or return figure
        
        Parameters
        ----------
        psi: complex numpi array
            psi field to which the vortex points will be added (by multiplication)
        filename: string (None)
            filename to save the figure to a file
        figsize: tuple ((8,8))
            size of figure in inches
        dpi: integer (200)
            dpi of image
        
        Returns
        -------
        image: image
            when no filename is specified it returns a pyplot figure object
    """
    
    fig=plt.figure(figsize=figsize)
    plt.imshow(np.transpose(np.square(np.abs(psi))), origin='lower')
    
    lx, ly=axes_labels
    plt.xlabel(lx)
    plt.ylabel(ly)
    
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
        plt.close('all')
    
    else:
        return fig




def phase_imshow2D(psi, filename=None, figsize=(8, 8), dpi=200, axes_labels=['nx', 'ny']):
    """
         Plot the pahse of psi and either save image or return figure
        
        Parameters
        ----------
        psi: complex numpi array
            psi field to which the vortex points will be added (by multiplication)
        filename: string (None)
            filename to save the figure to a file
        figsize: tuple ((8,8))
            size of figure in inches
        dpi: integer (200)
            dpi of image
        
        Returns
        -------
        image :image
            when no filename is specified it returns a pyplot figure object
    """

    fig=plt.figure(figsize=figsize)
    plt.imshow(np.transpose(np.angle(psi)), origin='lower')

    lx, ly=axes_labels
    plt.xlabel(lx)
    plt.ylabel(ly)

    if filename is not None:
        plt.savefig(filename, dpi=dpi)
        plt.close('all')

    else:
        return fig




def density_xSlice(psi, nxStar, filename=None, figsize=(8, 8), dpi=200, axes_labels=['ny', '|psi|^2']):
    """
        Plot the density of a slice at constant nxStar of psi and either save image or return figure
        
        Parameters
        ----------
        psi: complex numpi array
            psi field to which the vortex points will be added (by multiplication)
        nxStar: integer
            value between 0 and nx-1 at which the slice is cut (modulo nx) 
        filename: string (None)
            filename to save the figure to a file
        figsize: tuple ((8,8))
            size of figure in inches
        dpi: integer (200)
            dpi of image
        
        Returns
        -------
        image: image
            when no filename is specified it returns a pyplot figure object
    """
    
    fig=plt.figure(figsize=figsize)
    plt.plot(np.square(np.abs(psi[np.mod(nxStar, psi.shape[0]), :])))
    
    plt.xlabel(axes_labels[0])
    plt.ylabel(axes_labels[1])
    
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
        plt.close('all')
    
    else:
        return fig




def density_ySlice(psi, nyStar, filename=None, figsize=(8, 8), dpi=200, axes_labels=['nx', '|psi|^2']):
    """
        Plot the density of a slice at constant nxStar of psi and either save image or return figure
        
        Parameters
        ----------
        psi: complex numpi array
            psi field to which the vortex points will be added (by multiplication)
        nyStar: integer
            value between 0 and ny-1 at which the slice is cut (modulo ny) 
        filename: string (None)
            filename to save the figure to a file
        figsize: tuple ((8,8))
            size of figure in inches
        dpi: integer (200)
            dpi of image
        
        Returns
        -------
        image: image
            when no filename is specified it returns a pyplot figure object
    """
    
    fig=plt.figure(figsize=figsize)
    plt.plot(np.square(np.abs(psi[:, np.mod(nyStar, psi.shape[0])])))
    
    plt.xlabel(axes_labels[0])
    plt.ylabel(axes_labels[1])
    
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
        plt.close('all')
    
    else:
        return fig




def phase_xSlice(psi, nxStar, filename=None, figsize=(8, 8), dpi=200, axes_labels=['ny', '|psi|^2']):
    """
        Plot the phase of a slice at constant nxStar of psi and either save image or return figure
        
        Parameters
        ----------
        psi: complex numpi array
            psi field to which the vortex points will be added (by multiplication)
        nxStar: integer
            value between 0 and nx-1 at which the slice is cut (modulo nx) 
        filename: string (None)
            filename to save the figure to a file
        figsize: tuple ((8,8))
            size of figure in inches
        dpi: integer (200)
            dpi of image
        
        Returns
        -------
        image: image
            when no filename is specified it returns a pyplot figure object
    """
    
    fig=plt.figure(figsize=figsize)
    plt.plot(np.angle(psi[np.mod(nxStar, psi.shape[0]), :]))
    
    plt.xlabel(axes_labels[0])
    plt.ylabel(axes_labels[1])
    
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
        plt.close('all')
    
    else:
        return fig




def phase_ySlice(psi, nyStar, filename=None, figsize=(8, 8), dpi=200, axes_labels=['nx', '|psi|^2']):
    """
        Plot the phase of a slice at constant nxStar of psi and either save image or return figure
        
        Parameters
        ----------
        psi: complex numpi array
            psi field to which the vortex points will be added (by multiplication)
        nyStar: integer
            value between 0 and ny-1 at which the slice is cut (modulo ny) 
        filename: string (None)
            filename to save the figure to a file
        figsize: tuple ((8,8))
            size of figure in inches
        dpi: integer (200)
            dpi of image
        
        Returns
        -------
        image: image
            when no filename is specified it returns a pyplot figure object
    """
    
    fig=plt.figure(figsize=figsize)
    plt.plot(np.angle(psi[:, np.mod(nyStar, psi.shape[0])]))
    
    plt.xlabel(axes_labels[0])
    plt.ylabel(axes_labels[1])
    
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
        plt.close('all')
    
    else:
        return fig


