#
# UEA Quantum Fluids group
#
# Spectral integration of the two-dimensional Gross-Pitaevskii (GP) equation,
# i\frac{\partial\psi}{\partial t} + \beta\nabla\psi + \alpha|\psi|^2\psi = 0,
# with no external potential and periodic boundary conditions.
#
# Time integrator: first order split-step method.
# Boundary conditions: periodic
#


# Load python modules
import numpy as np


# Routines

def mass(psi, dV=1):
    """
        Compute the mass (L^2 norm) of the field psi
        
        Parameters
        ----------
        psi: complex numpi array
            psi field
        dV: double
            inifinitesimal volume element
        
        Returns
        -------
        mass: double
            mass (L^2 norm) of the field psi
    """
    
    return dV*np.sum(np.square(np.abs(psi)))




def linearEnergy(psi, kSquare, beta, dV=1):
    """
        Compute the mass (L^2 norm) of the field psi
        
        Parameters
        ----------
        psi: complex numpi array
            psi field
        kSquare: real numpi array
            |k|^2 array
        beta: double
            parameter in front of the linear energy term
        dV: double
            inifinitesimal volume element
        
        Returns
        -------
        linearEnergy: double
            linear energy of the field psi
    """
    
    psi=np.fft.fft2(psi)
    
    le = beta*np.sum(kSquare*np.abs(psi)**2)*dV

    return le




def nonlinearEnergy(psi, alpha, dV=1):
    """
        Compute the mass (L^2 norm) of the field psi
        
        Parameters
        ----------
        psi: complex numpi array
            psi field
        alpha: double
            parameter in front of the nonlinear energy term
        dV: double
            inifinitesimal volume element
        
        Returns
        -------
        nonlinearEnergy: double
            nonlinear energy of the field psi
    """
    
    return -(alpha/2.)*np.sum(np.abs(psi)**4)*dV




def totalEnergy(psi, kSquare, alpha, beta, dV=1):
    """
        Compute the mass (L^2 norm) of the field psi
        
        Parameters
        ----------
        psi: complex numpi array
            psi field
        kSquare: real numpi array
            |k|^2 array
        alpha: double
            parameter in front of the nonlinear energy term
        beta: double
            parameter in front of the linear energy term
        dV: double
            inifinitesimal volume element
        
        Returns
        -------
        nonlinearEnergy: double
            nonlinear energy of the field psi
    """
    
    return linearEnergy(psi, kSquare, beta, dV)+nonlinearEnergy(psi, alpha, dV)




def linearMomentum(psi, nx, ny, kx, ky, beta, dV=1):
    """
        Compute the mass (L^2 norm) of the field psi
        
        Parameters
        ----------
        psi: complex numpi array
            psi field
        kx: double numpi array
            kx array 
        ky: double numpi array
            ky array 
        beta: double
            parameter in front of the linear energy term
        dV: double
            inifinitesimal volume element
        
        Returns
        -------
        px, py: double numpi array
            linear momentum of the field psi
    """
    psi=np.fft.fft2(psi)
    
    px=beta*np.sum(np.kron(kx, np.ones(ny)).reshape(nx, ny)*np.abs(psi)**2)*dV
    py=beta*np.sum(np.kron(np.ones(nx), ky).reshape(nx, ny)*np.abs(psi)**2)*dV

    return np.array([px, py])




def chemicalPotential(psi, kSquare, alpha, beta, dV=1):
    """
        Parameters
        ----------
        psi: complex numpi array
            psi field
        kSquare: real numpi array
            |k|^2 array
        alpha: double
            parameter in front of the nonlinear energy term
        beta: double
            parameter in front of the linear energy term
        dV: double
            inifinitesimal volume element

        Returns
        ----------
        mu: double
            chemical potential 
    """

    mu = (linearEnergy(psi, kSquare, beta, dV) + 2.*nonlinearEnergy(psi, alpha, dV))/mass(psi, dV)

    return mu


def dipoleEnergy(psi, kSquare, beta, alpha, dV=1):
    """
        Parameters
        ----------
        psi: complex numpi array
            psi field
        kSquare: real numpi array
            |k|^2 array
        alpha: double
            parameter in front of the nonlinear energy term
        beta: double
            parameter in front of the linear energy term
        dV: double
            inifinitesimal volume element
            

        Returns
        ---------
        E: dipole energy
    """

    E = linearEnergy(psi, kSquare, beta, dV) - (alpha/2.)*np.sum((1 - np.abs(psi)**2)**2)*dV

    return E


def dipoleImpulse(psi, kSquare, beta, alpha, ux, dV=1):
    """
        Parameters
        ----------
        psi: complex numpi array
            psi field
        kSquare: real numpi array
            |k|^2 array
        alpha: double
            parameter in front of the nonlinear energy term
        beta: double
            parameter in front of the linear energy term
        u: double 
            the x-component of the dipole velocity. 
        dV: double
            inifinitesimal volume element
            

        Returns
        ---------
        px: double
            linear momentum of the dipole
    """  

    px = (1./ux)*(dipoleEnergy(psi, kSquare, beta, alpha, dV) + (alpha/2.)*np.sum((1 - np.abs(psi)**2)*(np.abs(1 - psi)**2))*dV)

    return px

