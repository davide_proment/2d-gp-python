# 2D GP Python

Python spectral code to integrate the two-dimensional Gross-Pitaevski equation.

Time integrator: first order split-step method
Boundary conditions: periodic

______________

Notes:
.gitignore created to avoid python compilation, outputs, and OS trash files